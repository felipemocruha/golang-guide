# golang-guide

blah blah blah
Go is a compiled, concurrent, garbage-collected, statically typed language.

## Why?

> Go was designed and developed to make working in this environment more productive. Besides its better-known aspects such as built-in concurrency and garbage collection, Go's design considerations include rigorous dependency management, the adaptability of software architecture as systems grow, and robustness across the boundaries between components.

> The goals of the Go project were to eliminate the slowness and clumsiness of software development (...) The language was designed by and for people who write—and read and debug and maintain—large software systems. 


## What?

## History

## Syntax

## Datastructures

## Write idiomatic code

## Package management

## Testing

## Concurrency

## Useful Libraries

## Read Good Code

## Toolchain

## Bundling

## Community

## Books

## Talks

## Pitfalls

## Side Project Ideas

## FAQ

